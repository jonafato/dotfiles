let g:syntastic_tex_checkers = ['chktex']
" Disable "Command terminated with space." error
let g:syntastic_tex_chktex_args = "-n1"

setlocal spell
