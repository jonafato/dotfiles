" setlocal omnifunc=jedi#completions
" Code should be no longer than 79 characters, comments and strings 72
setlocal colorcolumn=73,88
setlocal textwidth=87

" let g:jedi#auto_vim_configuration = 0
" let g:jedi#popup_on_dot = 0  " Don't autocomplete automatically
" let g:jedi#show_call_signatures = 2  " Preserve undo history integrity
" let g:jedi#smart_auto_mappings = 0  " Don't automatically insert `import` after `from foo`
" let g:jedi#usages_command = "<Leader>u" " I use <Leader>n for NerdTreeToggle
setlocal bs=2 " make backspace behave like normal again

" Highlight all the things
let python_highlight_all=1

" Finally!!!
call OnSyntaxChange#Install('Comment', '^Comment$', 0, 'a')
autocmd User SyntaxCommentEnterA setlocal textwidth=72
autocmd User SyntaxCommentLeaveA setlocal textwidth=87
call OnSyntaxChange#Install('PythonDocstring', '^PythonDocstring$', 0, 'a')
autocmd User SyntaxPythonDocstringEnterA setlocal textwidth=72
autocmd User SyntaxPythonDocstringLeaveA setlocal textwidth=87

let b:ale_linters = ['flake8']
