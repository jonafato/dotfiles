set nocompatible " be iMproved

call plug#begin()

" Functional plugins
" Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'dhruvasagar/vim-table-mode'  " For making tables easily
Plug 'dietsche/vim-lastplace'  " Pick up right where I left off
Plug 'djoshea/vim-autoread'  " Automatically read files from disk when they change
" Plug 'ervandew/supertab'  " Tab completion
Plug 'godlygeek/tabular'  " For handling tabular data
Plug 'gregsexton/MatchTag'  " Highlight matching html tags
Plug 'haya14busa/is.vim'  " incsearch improvements
Plug 'jmcantrell/vim-virtualenv'
Plug 'junegunn/goyo.vim'  " Distraction-free prose writing
Plug 'junegunn/limelight.vim'  " Goes with goyo
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'reedes/vim-pencil'
Plug 'mattn/emmet-vim'
Plug 'MarcWeber/vim-addon-local-vimrc'  " Safely and automatically load local vimrc files if they exist
Plug 'mbbill/undotree'  " Visualize undo history
Plug 'mhinz/vim-startify'  " Start screen
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'preservim/nerdcommenter'  " Quickly (un)comment code
Plug 'rhysd/committia.vim'  " Better git commits
" Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }  " Autocomplete
" Plug 'deoplete-plugins/deoplete-lsp'
Plug 'tpope/vim-fugitive'  " Git support
Plug 'vim-scripts/OnSyntaxChange'
Plug 'vim-scripts/Tabmerge'  " Move tabs to splits
Plug 'vim-scripts/TaskList.vim'  " Find todos scattered throughout code
Plug 'vim-voom/VOoM'
Plug 'w0rp/ale'  " Like async syntastic
" Plug 'zchee/deoplete-jedi'  " Autocomplete Python via deoplete

" UI enhancements
Plug 'altercation/vim-colors-solarized'  " Solarized, for happy eyes

" Additional language support
Plug 'arrufat/vala.vim'  " (neo)vim doesn't have Vala support out of the box
Plug 'chase/vim-ansible-yaml'  " Better handling of yaml for ansible playbooks
Plug 'chooh/brightscript.vim'  " Roku Brightscript syntax highlighting
Plug 'blankname/vim-fish'  " fish shell syntax highlighting and helpers
Plug 'ledger/vim-ledger'  " Ledger accounting format
Plug 'lepture/vim-jinja'  " Jinja syntax
Plug 'neovim/nvim-lspconfig'
Plug 'nvim-lua/completion-nvim'
Plug 'othree/html5.vim'  " Newer html syntax support
Plug 'wting/rust.vim'  " Rust syntax support
Plug 'zah/nimrod.vim'  " Nim syntax support
Plug 'RishabhRD/popfix'
Plug 'RishabhRD/nvim-lsputils'

call plug#end()

" Use , instead of \
let mapleader=","

augroup markdown_filetype
    autocmd BufNewFile,BufReadPost *.md set filetype=markdown  " Properly identify markdown files
augroup END

let g:vim_markdown_folding_disabled=1
set nofoldenable

let g:SuperTabDefaultCompletionType="context"
" let g:deoplete#enable_at_startup = 1
" call deoplete#custom#option({
"     \ 'auto_complete_popup': "manual",
" \ })

" Use Ctrl-t to open a new tab with startify
map <C-t> :tabnew<CR>:Startify<CR>
let g:startify_change_to_dir=0  " Don't change to the directory of the file being opened
let g:startify_change_to_vcs_root=1  " Always start from the root of the repository (if applicable)
" This order makes more sense to me
let g:startify_list_order=[[' Sessions'], 'sessions', [' Current Directory'], 'dir', [' All Files'], 'files', [' Bookmarks'], 'bookmarks']

" Use Ctrl-p to search for files with fzf
map <C-p> :Files .<CR>

" Code highlighting in fenced codeblocks
let g:markdown_fenced_languages = ['json']

let g:committia_min_window_width = 120

let g:python3_host_prog = "/usr/bin"

set wildignore+=*.pyc,*/*.egg-info/*  " Python incidentals
set wildignore+=node_modules  " npm installed files
set wildignore+=.git,.hg  " Source control
set wildignore+=*.sw?  " Swap files
set wildignore+=*.jpg,*.jpeg,*.gif,*.png  " Images
set wildignorecase  " Case-insensitive completions

set laststatus=2  " Always show airline
set noshowmode
let g:airline_powerline_fonts = 1  " Use powerline fonts with airline
let g:airline_theme = 'solarized'  " Solarized colors!
let g:airline#extensions#tabline#enabled = 1  " Airline tabline
let g:airline#extensions#virtualenv#enabled = 1  " Virtualenv display
let g:airline#extensions#tabline#tab_nr_type = 2 " Show number of splits and tab number

let g:ale_sign_column_always = 1  " Always show the sign column to prevent jitter
let g:ale_fix_on_save = 1
let g:ale_fixers = {
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\}

let g:NERDSpaceDelims = 1

syntax on  " Syntax highlighting
set bg=light  " Light background
let alacritty_colorscheme = trim(system('cat $HOME/.config/alacritty/alacritty.yml | grep "colors:" | cut -d "*" -f 2'))
if alacritty_colorscheme == "dark"
    set bg=dark
endif
let g:solarized_termtrans=1
silent! colorscheme solarized  " Solarized colors (if available, ignored otherwise)!

set spell

set cursorline
set number
set relativenumber
set backspace=indent,eol,start
set tabstop=4  " Tabs are 4 spaces wide
set shiftwidth=4  " Shifting 4 spaces at a time
set smarttab
set expandtab  " Tabs convert to spaces
set softtabstop=4  " Tabs are 4 actual spaces
set printoptions=paper:A4,syntax:y,wrap:y,number:y  " When do I print?

set clipboard=unnamedplus  " Use the system clipboard
" set completeopt-=preview
set completeopt=menuone,noinsert,noselect
let g:completion_enable_auto_popup = 0
imap <tab> <Plug>(completion_smart_tab)
imap <s-tab> <Plug>(completion_smart_s_tab)

" Make searching better
set incsearch
set ignorecase
set smartcase

" Highlight search results when searching, clear highlights after.
set hlsearch
" augroup vimrc-incsearch-highlight
"   autocmd!
"   autocmd CmdlineEnter /,\? :set hlsearch
"   autocmd CmdlineLeave /,\? :set nohlsearch
" augroup END

set wildmenu

" No shift needed
nnoremap ; :
" No shift needed
vnoremap ; :
set scrolloff=2

" Move within visual lines when wrapped
nnoremap <Down> gj
nnoremap <Up> gk
vnoremap <Down> gj
vnoremap <Up> gk
inoremap <Down> <C-o>gj
inoremap <Up> <C-o>gk

" Mouse support everywhere
set mouse=a

let g:limelight_conceal_ctermfg = 'gray'
let g:limelight_conceal_ctermfg = 240

lua << EOF
local nvim_lsp = require'lspconfig'.jedi_language_server.setup{on_attach=require'completion'.on_attach}
require('nvim-treesitter.configs').setup {
  ensure_installed = 'maintained', -- one of "all", "maintained" (parsers with maintainers), or a list of languages
--  highlight = {
--    enable = true,
--  },
--  indent = {
--    enable = true,
--  }
}
vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
  vim.lsp.diagnostic.on_publish_diagnostics, {
    virtual_text = true,
    underline = true,
    signs = true,
  }
)
vim.lsp.handlers['textDocument/codeAction'] = require'lsputil.codeAction'.code_action_handler
vim.lsp.handlers['textDocument/references'] = require'lsputil.locations'.references_handler
vim.lsp.handlers['textDocument/definition'] = require'lsputil.locations'.definition_handler
vim.lsp.handlers['textDocument/declaration'] = require'lsputil.locations'.declaration_handler
vim.lsp.handlers['textDocument/typeDefinition'] = require'lsputil.locations'.typeDefinition_handler
vim.lsp.handlers['textDocument/implementation'] = require'lsputil.locations'.implementation_handler
vim.lsp.handlers['textDocument/documentSymbol'] = require'lsputil.symbols'.document_handler
vim.lsp.handlers['workspace/symbol'] = require'lsputil.symbols'.workspace_handler
-- vim.cmd [[autocmd CursorMoved * lua vim.lsp.diagnostic.show_line_diagnostics()]]
-- vim.cmd [[autocmd CursorMovedI * silent! lua vim.lsp.buf.signature_help()]]
EOF

" Store undo history across sessions
"set undofile

set autoread
augroup autoautoread
    autocmd BufEnter * :checktime
augroup END
