#!/bin/sh
# Install nix configuration
# sudo ln -s $(pwd)/configuration.nix /etc/nixos/configuration.nix
# sudo nixos-rebuild boot --upgrade

# Install dotfiles
ln -s $(pwd)/nvim ~/.config/nvim
ln -s $(pwd)/tmux.conf ~/.tmux.conf
ln -s $(pwd)/fish ~/.config/fish
ln -s $(pwd)/alacritty ~/.config/alacritty
