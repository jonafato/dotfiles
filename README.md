## Dotfiles

A constant work in progress


### Installing

Warning! This is *very* specific to my personal laptop right now. Trying to run this on a different
machine may break something. If that happens, boot into your previous generation and run
`nixos-rebuild switch --rollback`. To proceed anyway:

    ./bootstrap.sh

### Contributing

If you see something that could be done better, feel free to raise an issue or merge request.
