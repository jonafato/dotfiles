set -x EDITOR nvim
set -x DIRENV_LOG_FORMAT ""

# Fish git prompt
set __fish_git_prompt_showdirtystate 'yes'
set __fish_git_prompt_showstashstate 'yes'
set __fish_git_prompt_showuntrackedfiles 'yes'
set __fish_git_prompt_showupstream 'yes'
set __fish_git_prompt_color_branch yellow
set __fish_git_prompt_color_upstream_ahead green
set __fish_git_prompt_color_upstream_behind red

# Status Chars
set __fish_git_prompt_char_dirtystate '±'
set __fish_git_prompt_char_stagedstate '●'
set __fish_git_prompt_char_untrackedfiles '⊕'
set __fish_git_prompt_char_stashstate '↩'
set __fish_git_prompt_char_conflictedstate '☢'
set __fish_git_prompt_char_upstream_ahead '↑'
set __fish_git_prompt_char_upstream_behind '↓'

# Useful aliases
alias gco "git checkout"
alias rg "rg -S"

# Clear out the default new shell prompt
set fish_greeting

#eval (direnv hook fish)

if functions -q fundle
    fundle plugin 'edc/bass'
    fundle init
end

#if type -q vf
   # vf install auto_activation compat_aliases global_requirements
#end

# pyenv init
set -Ux PYENV_ROOT $HOME/Projects/pyenv
fish_add_path --prepend $PYENV_ROOT/bin
set -Ux VIRTUALFISH_HOME $HOME/.virtualenvs
if command -v pyenv 1>/dev/null 2>&1
  status is-login; and pyenv init --path | source
  pyenv init - | source
end

set -Ux RBENV_ROOT $HOME/Projects/rbenv
fish_add_path --prepend $RBENV_ROOT/bin
if command -v rbenv 1>/dev/null 2>&1
  status --is-interactive; and rbenv init - | source
end

set -Ux NODENV_ROOT $HOME/Projects/nodenv
fish_add_path --prepend $NODENV_ROOT/bin
if command -v nodenv 1>/dev/null 2>&1
  status --is-interactive; and nodenv init - | source
end

fish_add_path --prepend $HOME/.local/bin $HOME/.cargo/bin
