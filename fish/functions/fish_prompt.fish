function fish_prompt
	set last_status $status

    # Include virtualenv info
    if set -q VIRTUAL_ENV
        echo -n -s "(" (basename "$VIRTUAL_ENV") ") "
    end


    # Include git info
    set_color $fish_color_cwd
    printf '%s' (prompt_pwd)
    set_color normal

    printf '%s' (__fish_git_prompt) ' » '

    set_color normal
end
