# Defined in /tmp/fish.fLfITS/nr.fish @ line 1
function nr --description 'Run a program using nix run'
    nix run -f '<nixpkgs>' $argv[1] --command $argv
end
