# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      /etc/nixos/hardware-configuration.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.grub.device = "/dev/sda";
  boot.loader.grub.copyKernels = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.efiSupport = true;
  boot.kernelParams = [ "elevator=noop" "boot.shell_on_fail" ];
  boot.zfs.requestEncryptionCredentials = true;
  boot.supportedFilesystems = ["zfs"];
  boot.tmpOnTmpfs = true;

  services.zfs.autoSnapshot.enable = true;
  services.zfs.autoScrub.enable = true;
  services.zfs.trim.enable = true;

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.hostId = "bb1d7fb2";
  networking.hostName = "flexo";
  networking.useDHCP = false;
  networking.interfaces.enp0s31f6.useDHCP = true;
  networking.interfaces.wlp4s0.useDHCP = true;

  hardware.bluetooth.enable = true;
  hardware.pulseaudio = {
    enable = true;

    # NixOS allows either a lightweight build (default) or full build of PulseAudio to be installed.
    # Only the full build has Bluetooth support, so it must be selected here.
    package = pkgs.pulseaudioFull;
  };

  # Set your time zone.
  time.timeZone = "America/New_York";

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # Use hydra's build caches
  nix.trustedBinaryCaches = [ "http://hydra.nixos.org" ];

  # Swap as little as possible
  boot.kernel.sysctl = { "vm.swappiness" = 1; };

  # Use the latest Linux kernel
  boot.kernelPackages = pkgs.linuxPackages_latest;

  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget
  environment.systemPackages = with pkgs; [
    alacritty
    albert
    calibre
    direnv
    discord
    docker_compose
    firefox-beta-bin
    git
    google-chrome
    gnome3.gnome-disk-utility
    gnome3.gnome-tweaks
    gnome3.polari
    gnomeExtensions.appindicator
    gnomeExtensions.caffeine
    gnomeExtensions.clipboard-indicator
    #gnomeExtensions.icon-hider
    gnomeExtensions.night-theme-switcher
    gnomeExtensions.remove-dropdown-arrows
    #gnomeExtensions.system-monitor
    #gnomeExtensions.taskwhisperer
    gnupg
    gnupg1compat
    hunspellDicts.en-us
    htop
    keepassx-community
    libnotify
    #libreoffice-fresh
    lsof
    neovim
    newsflash
    nfs-utils
    nixUnstable
    obs-studio
    openssl
    paperwork
    postgresql
    pypi2nix
    python38
    ripgrep
    seafile-client
    shotwell
    signal-desktop
    simple-scan
    slack
    taskwarrior
    timewarrior
    tmux
    torbrowser
    tree
    universal-ctags
    vim
    wget
    which
    wireguard-tools
    xclip
    xwayland
    zulip
  ];

  programs.fish.enable = true;

  environment.gnome3.excludePackages = with pkgs.gnome3; [
    evolution
    gnome-software
  ];

  fonts = {
    enableFontDir = true;
    enableGhostscriptFonts = true;
    fonts = with pkgs; [
      twemoji-color-font
      powerline-fonts
    ];
  };

  # Don't build the manual
  documentation.nixos.enable = false;

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Keyboard configuration
  services.xserver.layout = "us";
  services.xserver.xkbOptions = "ctrl:nocaps";

  # Trackpad configuration
  services.xserver.libinput = {
    enable = true;
    naturalScrolling = false;
  };

  # Enable the Gnome Desktop Environment.
  services.xserver.displayManager.gdm.enable = true;
  services.xserver.displayManager.gdm.wayland = true;
  services.xserver.desktopManager.gnome3.enable = true;

  # Power saving
  services.tlp.enable = true;

  services.lorri.enable = true;

  services.ipfs = {
    enable = true;
    gatewayAddress = "/ip4/127.0.0.1/tcp/8888";
  };

  # Yubikey
  services.udev.packages = [
    pkgs.libu2f-host
    pkgs.yubikey-personalization
  ];

  services.gnome3.chrome-gnome-shell.enable = false;

  # Use Docker
  virtualisation = {
    #anbox.enable = true;
    docker = {
      enable = true;
      storageDriver = "zfs";
    };
    #virtualbox.host.enable = true;
  };

  environment.shells = with pkgs; [ bash fish ];

  # Printing!
  services.printing = {
    enable = true;
    drivers = [ pkgs.gutenprint pkgs.hplipWithPlugin ];
  };

  users.extraUsers.jonafato = {
    name = "jonafato";
    description = "Jon Banafato";
    group = "users";
    extraGroups = [
      "wheel" "disk" "audio" "video" "docker"
      "networkmanager" "scanner" "systemd-journal"
    ];
    createHome = true;
    uid = 1000;
    home = "/home/jonafato";
    shell = "/run/current-system/sw/bin/fish";
  };

  # The NixOS release to be compatible with for stateful data such as databases.
  system.stateVersion = "unstable";
}
